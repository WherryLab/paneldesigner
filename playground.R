library(shiny)

  ui <- fluidPage(
    column(4,
           numericInput("x", "Value", 5),
           br(),
           actionButton("button", "Show")
    ),
    column(8, tableOutput("table"))
  )
  server <- function(input, output) {
    # Take an action every time button is pressed;
    # here, we just print a message to the console
    observeEvent(input$button, {
      cat("Showing", input$x, "rows\n")
    })
    # Take a reactive dependency on input$button, but
    # not on any of the stuff inside the function
    df <- eventReactive(input$button, {
      head(cars, input$x)
    })
    output$table <- renderTable({
      df()
    })
  }
  shinyApp(ui=ui, server=server)

  
library(foreach); library(doParallel)
num_cores <- detectCores()-1

  E = sapply(1:10000, function(n) {max.eig(5, 1)})
  cl<-makeCluster(num_cores)
  registerDoParallel(cl)
#  registerDoParallel(4)  
  system.time(foreach(n = 1:250) %dopar% max.eig(n, 1))
  stopCluster(cl)
  
  max.eig <- function(N, sigma) {
     d <- matrix(rnorm(N**2, sd = sigma), nrow = N)
     E <- eigen(d)$values
     abs(E)[[1]]
   }  

  
  base <- 2
  cl<-makeCluster(2)
  registerDoParallel(cl)
  foreach(exponent = 2:6, .combine = c)  %dopar%  base^exponent
  stopCluster(cl)  
  
  
  
  
  
  
  
  
  calculatePenalty_foreach <- function(myPanel,ssmSubset, inList)
  {
    Penalty <- numeric() # temp dataframe to calculate penalty scores
    hiPriority <- 0; lowPriority <- 0;
    print("calculating penalties")
    result <- foreach(x=1:nrow(myPanel),.combine=rbind ) %do%  {
      for(j in 1:nrow(myPanel))  # where j represents all of the other antibodies in myPanel
      { 
        Penalty[j] <- 0.5*ssmSubset[x,j] * myPanel[j,2] * myPanel[j,7] - 
          myPanel[j,7]*inList[myPanel$Protein[x],myPanel$Protein[j]] - # bonus if mutually exclusive
          lowPriority*sum(ssmSubset[x,]) -   # bonus if lowPriority on messy ch  
          hiPriority*300/(0.1+sum(ssmSubset[x,])) +  # bonus if hiPriority on good ch
          lowPriority*( myPanel[j,2] * myPanel[j,7] ) # penalize low priority & bright on strong channel
        Penalty[j] <- round(Penalty[j])
      }
      return(Penalty)
    } 
    result
    #penaltyResult <- data.frame(x1=length(result),x2=)
    return(result)
  }
  
  system.time(Penalty2 <- calculatePenalty_foreach(myPanel,ssmSubset, inList))
  
  
  