
##  ****************** import BD inventory  *********************

fluorNames <- function(x) 
{
  x <- gsub(pattern="BUV563",replacement="BUV615-A",x)  ## note 563 converted to 615
  x <- gsub(pattern="PE-Cy™5",replacement="PE-Cy5",x)
  x <- gsub(pattern="PE-Cy5.5",replacement="PE-Cy55",x) 
  x <- gsub(pattern="PE-Cy™7",replacement="PE-Cy7",x)
  x <- gsub(pattern="^PerCP$",replacement="PerCP-Cy55",x)
  x <- gsub(pattern="PerCP-Cy™5.5",replacement="PerCP-Cy55",x)
  x <- gsub(pattern="APC-Cy™7",replacement="APC-Cy7",x)
}



importInventory <- function() {
  #browser()
  BDinventory <- read.csv(file="../../inventory/combinedInventory.csv",stringsAsFactors = FALSE, strip.white = TRUE)
  
  renamedInventory <- as.data.frame(sapply(BDinventory, fluorNames))
  renamedInventory <- unique(renamedInventory)
  renamedInventory <- renamedInventory[which(renamedInventory$Color != "Purified"),]
  
#  indexInventory <- unique(renamedInventory$Target)
#  write.csv(indexInventory,file="inventory/indexInventory.csv")
  return(renamedInventory)
}

inventoryLocal <- function(markerInfo) {
  ## *******************************************************  local antibody inventory  
  #browser()
  inventorytable <- read.csv(file="inventory-human.csv",stringsAsFactors = FALSE,header = FALSE, strip.white = TRUE)
  rownames(inventorytable) <- inventorytable$V1; inventorytable$V1 <- NULL
  localInventory <- list()
  for (i in 1:nrow(inventorytable))
  {
    tempRow <- inventorytable[i,]
    indices <- which(tempRow != "NA" & tempRow != "")
    localInventory[[i]] <- inventorytable[i,indices]
  }; names(localInventory) <- rownames(inventorytable);
  inventorytable <- validateInventory(localInventory,markerInfo) 
  return(inventorytable)
}

inventoryCommercial <- function(markerInfo) {
  ## ********************************************************* bring in commercial inventory
  #browser()
  commercialInventory <- importInventory()  # commercialInventory <- renamedInventory
  library(reshape2)
  molten <- melt(data=commercialInventory,id.vars="Target")
  shaped <- dcast(molten,Target~value)
  #saveNames <- as.character(shaped$Target)
  rownames(shaped) <- shaped$Target; shaped$Target <- NULL
  commercialInventory <- list()
  for (i in 1:nrow(shaped))
  {
    tempRow <- shaped[i,]
    indices <- which(tempRow != "NA")
    commercialInventory[[i]] <- shaped[i,indices]
  } 
  names(commercialInventory) <- rownames(shaped) #saveNames
  inventorytable <- validateInventory(commercialInventory,markerInfo) 
  return(inventorytable)
}


validateInventory<- function(inventorytable,markerInfo) 
{
  print("validating inventory"); #browser()
  fluors <- data.frame(markerInfo[[5]]); rownames(fluors) <- fluors$Fluorochrome
  for (i in 1:length(inventorytable))
  {
    if(i>length(inventorytable)) break
    for (j in 1:length(inventorytable[[i]]))
    {
      if(j>length(inventorytable[[i]])) next
      if (as.character(inventorytable[[i]][j]) != "")
      { 
        if(is.na(fluors[as.character(inventorytable[[i]][j]),1]))    
        { 
          #print(c("bad fluor name, check spellings", inventorytable[[i]][j] )) 
          inventorytable[[i]] <- inventorytable[[i]][-j]  # trim for now
          next 
        }
      }
    }
    if(length(inventorytable[[i]]) == 0)
      inventorytable <- inventorytable[-i]
  } 
  return(inventorytable)
}



## ************* replace fluor names with numbers *****************
convertInventory <- function(inventorytable,markerInfo)    
{
  print("convert inventory to fluor numbers")
  channels <- markerInfo[[4]]
  fluors <- data.frame(markerInfo[[5]]); rownames(fluors) <- fluors$Fluorochrome
  for (i in 1:length(inventorytable))
  {
    for (j in 1:length(inventorytable[[i]]))   # parse each cell in the list
    {
      if (as.character(inventorytable[[i]][j]) == "")
      {inventorytable[[i]][j] <- NA; next}   # if blank then go to next iteration
      inventorytable[[i]][j] <- which(rownames(fluors) == as.character(inventorytable[[i]][j]))   
                # replace fluor text name with number
    }
  }  
  return(inventorytable)
}

## ************* assign fluors to channels  *****************
assignFluorToChannel <- function (fluors)
{
  fluorEquiv <- read.csv(file="Channels-Fluors-equivalency.csv", stringsAsFactors = FALSE)
  rownames(fluorEquiv) <- fluorEquiv$Channel; fluorEquiv$Channel <- NULL
  fluorToChannel <- list()
  for (i in 1:nrow(fluorEquiv))
  {
    tempRow <- fluorEquiv[i,]
    indices <- which(tempRow != "NA" & tempRow != "")
    fluorToChannel[[i]] <- fluorEquiv[i,indices]
  }; names(fluorToChannel) <- rownames(fluorEquiv);
  
  ## now convert fluors to numbers
  
  for (i in 1:length(fluorToChannel))
  {
    for (j in 1:length(fluorToChannel[[i]]))   # parse each cell in the list
    {
      if ( length(fluorToChannel[[i]]) == 1  )  # if no alternative fluors listed
      { fluorToChannel[[i]][j] <- which(rownames(fluors) == as.character(fluorToChannel[[i]][j])); next }   # if blank then go to next iteration
      fluorToChannel[[i]][j] <- which(rownames(fluors) == as.character(fluorToChannel[[i]][j]))   
      # replace fluor text name with number
    }
  }  
  return(fluorToChannel)
}
